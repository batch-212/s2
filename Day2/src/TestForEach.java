public class TestForEach {

    public static void main(String[] args) {

        int[] arrOfNum = {1,2,3,4,5,6,7,8,10};
        int sum = 0;

        for (int num: arrOfNum) {
            sum += num;
        }

        System.out.println(sum);

    }

}
