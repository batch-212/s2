import java.lang.Math;
import java.math.BigDecimal;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {

//        double value = Math.PI;
//        System.out.println(value);
//
//        BigDecimal tmp = new BigDecimal(Double.toString(value));
//        tmp = tmp.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//        String strValue = tmp.stripTrailingZeros().toPlainString();
//
//
//        System.out.println(strValue);


//        double num = 0.999939;
//        System.out.println(num);


//        double num1 = 9, num2 = 2;
//        double total = num1 / num2;
//
//        System.out.println(total);


//        int num1 = 10;
//        double num2 = 10;
//
//        System.out.println(num1);
//        System.out.println(num2);
//        System.out.println(num1 == num2);


//        int num1 = 0;
//
//        System.out.println(num1++);
//        System.out.println(num1++);


//        instanceof
//        String name = "Charles";
//        boolean result = name instanceof String;
//
//        System.out.println(result);

//        Input
        Scanner input = new Scanner(System.in);

//        System.out.println("Enter first name:");
//        String firstName = input.nextLine();
//
//        System.out.println("Enter middle name:");
//        String middleName = input.nextLine();
//
//        System.out.println("Enter last name:");
//        String lastName = input.nextLine();
//
//        System.out.println("Enter age:");
//        int age = input.nextInt();
//        input.nextLine();
//        //  this inserts a new line for the next input to be accepted
//        //  if there is an expected string input after an int input, insert nextLine()
//
//        System.out.println("Enter position");
//        String position = input.nextLine();
//
//        input.close();
//
//        System.out.println("Your full name is " + (firstName + " " + middleName.charAt(0) + ". " + lastName + "."));
//        System.out.println("You are " + age + " years old.");
//        System.out.println("You are a " + position + " in this company.");

        int num1 = input.nextInt(), num2 = input.nextInt();
        System.out.println(num1 + num2);

        /*
            This is a multi line comment
         */

    }

}
