import java.util.Scanner;

public class TestIfElse {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter math grade:");
        double mathGrade = input.nextDouble();

        System.out.println("Enter english grade:");
        double englishGrade = input.nextDouble();

        System.out.println("Enter art grade:");
        double artGrade = input.nextDouble();

        input.close();


        double result = (mathGrade + englishGrade + artGrade) / 3;
        String formattedResult = String.format("%.2f", result) + "%";

        if (result >= 90 && result <= 100) {
            System.out.println("Your grade is A: " + formattedResult);
        }

        else if (result >= 75 && result < 90) {
            System.out.println("Your grade is B: " + formattedResult);
        }

        else if (result >= 65 && result < 75) {
            System.out.println("Your grade is C: " + formattedResult);
        }

        else {
            System.out.println("Your grade is very low. You need to retake.");
        }


    }

}
