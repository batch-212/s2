import java.util.Scanner;

public class TestSwitchCase {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        double num1, num2, result;
        char operator;

        System.out.println("Enter first number:");
        num1 = input.nextInt();
        input.nextLine();

        System.out.println("Enter operator (+, -, *, /)");
        operator = input.nextLine().charAt(0);

        System.out.println("Enter second number:");
        num2 = input.nextInt();

        input.close();


        switch(operator) {

            case '+':
                result = num1 + num2;
                System.out.println(num1 + " + " + num2 + " = " + result);
                break;

            case '-':
                result = num1 - num2;
                System.out.println(num1 + " - " + num2 + " = " + result);
                break;

            case '*':
                result = num1 * num2;
                System.out.println(num1 + " * " + num2 + " = " + result);
                break;

            case '/':
                result = num1 / num2;
                System.out.println(num1 + " / " + num2 + " = " + result);
                break;

            default:
                System.out.println("Invalid operator! 😠");
                break;

        }

    }

}
