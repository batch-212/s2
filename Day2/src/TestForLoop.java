import java.sql.SQLOutput;
import java.util.Objects;
import java.util.Scanner;

public class TestForLoop {

    public static void main(String[] args) {

//        string1 == string2 - tests for reference equality (whether they are the same object).
//        String str1 = "test", str2 = "test";
//        System.out.println(str1 == str2);
//        System.out.println(str1 == "test");
//        System.out.println(str1 == new String("test"));

//        string1.equals(string2) - tests for value equality (whether they are logically "equal").
//        String string1 = "test", string2 = "test", string3 = null;
//        System.out.println(string1.equals(string2));
//        System.out.println(string1.equals("test"));
//        System.out.println(string1.equals(new String("test")));
//        System.out.println(string1.equals(string3));

//        Objects.equals(string1, string2) - checks for null before calling .equals() so you don't have to
//        String word1 = "test", word2 = "test", word3 = null;
//        System.out.println(Objects.equals(word1, word2));
//        System.out.println(Objects.equals(word1, word3));


        Scanner input = new Scanner(System.in);

        int range;
        String option;

        System.out.println("\n#### PRINT EVEN/ODD NUMBERS IN A GIVEN RANGE ####");

        System.out.println("Enter a range:");
        range = input.nextInt();
        input.nextLine();

        System.out.println("Enter option: [even/odd]");
        option = input.nextLine();

        input.close();

        for (int i = 1; i <= range; i++) {

            if (option.equals("even")) {
                if (i % 2 == 0) {
                    System.out.println("Even number: " + i);
                }
            }

            else if (option.equals("odd")) {
                if (i % 2 != 0) {
                    System.out.println("Odd number: " + i);
                }
            }

            else {
                System.out.println("[Invalid Operator] Will print all numbers in range.: " + i);
            }
        }

        if (option.equals("even")) {
            for (int i = 1; i <= range; i++) {
                if (i % 2 == 0) {
                    System.out.println("Even number: " + i);
                }
            }
        }

        else if (option.equals("odd")) {
            for (int i = 1; i <= range; i++) {
                if (i % 2 != 0) {
                    System.out.println("Odd number: " + i);
                }
            }
        }

        else {
            System.out.println("Invalid option");
        }

    }

}
