package com.problem.arrays;
import java.util.Arrays;

public class Problem2 {

    public static void main(String[] args) {

        /*

            Problem 2
                - Create a multidimensional array of 3
                - List your favorite singers/band and print all the array

         */

        String[][] favorite =
                {{"Never Run"}, {"CHNDTR"}, {"I Belong To The Zoo"}};

        System.out.println("\nPrint the whole array (default)");
        System.out.println(favorite);

        System.out.println("\nWay to display the whole array");
        System.out.println(Arrays.deepToString(favorite));

        System.out.println("\nAccessing each name in the array");
        for (String[] arr: favorite) {
            for (String band: arr) {
                System.out.println(band);
            }
        }
    }

}
