package com.problem.arrays;

import java.util.Arrays;

public class Problem1 {

    public static void main(String[] args) {

        /*

            Problem 1
                - Create a string array with a list of names, print out all the array

         */

        String[] names = {"Charles", "Kyla", "Djanin", "Beatrice","Nick"};

        System.out.println("\nPrint the whole array (default)");
        System.out.println(names);

        System.out.println("\nWay to display the whole array");
        System.out.println(Arrays.toString(names));

        System.out.println("\nAccessing each name in the array");
        for (String name: names) {
            System.out.println(name);
        }

    }

}
