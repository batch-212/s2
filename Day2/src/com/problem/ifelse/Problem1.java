package com.problem.ifelse;

import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {

        /*

            Problem 1
                - Design a program which determines if the supply age of the use is qualified to vote or not.
                - The qualifying age is 18 years old and above

         */

        Scanner input = new Scanner(System.in);

        System.out.println("Enter age:");
        int age = input.nextInt();
        input.close();

        // if the input is 18 and above
        if (age >= 18) {
            System.out.println("Your age is " + age + ". You are now qualified to vote.");
        }

        // if the input is below 18
        else {
            // tell the user how many more years he/she will wait before voting
            int yearsRemainingToVote = 18 - age;
            System.out.println("Your age is " + age + ". You need " + yearsRemainingToVote + " more years to be able to vote.");
        }

    }

}
