package com.problem.ifelse;

import java.util.Scanner;

public class problem4 {

    public static void main(String[] args) {

        /*

            Problem 4
                - Write a program that checks if the civil status of a person either the person is
                    > SINGLE
                    > MARRIED
                    > ANNULLED
                    > SEPARATED
                    > WIDOW

         */

        Scanner input = new Scanner(System.in);

        System.out.println("Enter civil status:");
        String civilStatus = input.nextLine().toUpperCase();
        input.close();

        if (civilStatus.equals("SINGLE")) {
            System.out.println("You are single 👍🏻");
        }

        else if (civilStatus.equals("MARRIED")) {
            System.out.println("You are married 💍");
        }

        else if (civilStatus.equals("ANNULLED")) {
            System.out.println("You are annulled 📜");
        }

        else if (civilStatus.equals("SEPARATED")) {
            System.out.println("You are separated 😐");
        }

        else if (civilStatus.equals("WIDOW")) {
            System.out.println("You are widowed 😭");
        }

        else {
            System.out.println("Invalid civil status!");
        }

    }

}
