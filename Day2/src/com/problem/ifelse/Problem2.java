package com.problem.ifelse;

import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {

        /*

            Problem 2
                - Design a program that determines if the input number is equal to secret words: "I love you!" or not.
                - Display result "I love you!" if the input is 143 and "Wrong Guess!!!" if its not

         */

        Scanner input = new Scanner(System.in);

        System.out.println("Enter a number guess:");
        int numGuess = input.nextInt();
        input.close();

        if (numGuess == 143) {
            System.out.println("I love you!");
        }

        else {
            System.out.println("Wrong Guess!!!");
        }

    }

}
