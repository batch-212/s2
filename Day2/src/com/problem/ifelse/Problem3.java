package com.problem.ifelse;

import java.util.Scanner;

public class Problem3 {

    public static void main(String[] args) {

        /*

            Problem 3
                - Write a program that checks if the user age is older than 18 but younger than 40.`
                - The second condition is "less than 50" (user < 50)
                - Finally if the age of user is greater than 50 years old

         */

        Scanner input = new Scanner(System.in);

        System.out.println("Enter your age:");
        int age = input.nextInt();
        input.close();

        if (age > 18 && age < 40) {
            System.out.println("Age category A (19-39)");
        }

        else if (age >= 40 && age < 50) {
            System.out.println("Age category B (40-49)");
        }

        else if (age >= 50) {
            System.out.println("Age category C (50 above)");
        }

        else {
            System.out.println("Age category D (minors)");
        }


    }

}
