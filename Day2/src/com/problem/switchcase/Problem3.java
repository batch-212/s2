package com.problem.switchcase;

import java.util.Scanner;

public class Problem3 {

    public static void main(String[] args) {

        /*

            Problem 3
                - Write a program that will ask the user to input a character "m" or "f" and then it will display
                    > "Hello Sir" if the input is "m"
                    > "Hello Madam" if the input is "f"

         */

        Scanner input = new Scanner(System.in);

        System.out.println("Enter gender [m/f]:");
        char gender = input.nextLine().charAt(0);

        switch (gender) {

            case 'm':
                System.out.println("Hello Sir!");
                break;

            case 'f':
                System.out.println("Hello Madam!");
                break;

            default:
                System.out.println("Invalid gender");
                break;

        }

    }

}
