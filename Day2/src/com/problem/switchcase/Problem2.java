package com.problem.switchcase;

import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {

        /*

            Problem 2
                - Write a program that will allow the user to select a day of the week with the following selection values
                    > 1-Monday
                    > 2-Tuesday
                    > 3-Wednesday
                    > 4-Thursday
                    > 5-Friday
                    > 6-Saturday
                    > 7-Sunday

         */

        Scanner input = new Scanner(System.in);

        System.out.println("Select day [1,2,3,4,5,6,7]:");
        int dayInNum = input.nextInt();
        input.close();

        switch (dayInNum) {

            case 1:
                System.out.println("Monday");
                break;

            case 2:
                System.out.println("Tuesday");
                break;

            case 3:
                System.out.println("Wednesday");
                break;

            case 4:
                System.out.println("Thursday");
                break;

            case 5:
                System.out.println("Friday");
                break;

            case 6:
                System.out.println("Saturday");
                break;

            case 7:
                System.out.println("Sunday");
                break;

            default:
                System.out.println("Invalid Day Try Again");
                break;

        }

    }

}
