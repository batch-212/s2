package com.problem.switchcase;

import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {

        /*

            Problem1
                - Design a program to display the year level of students based on their year entry number
                    > 1-FRESHMAN
                    > 2-SOPHOMORE
                    > 3-JUNIOR
                    > 4-SENIOR

         */

//        Scanner input = new Scanner(System.in);
//
//        System.out.println("Enter year level in number [1,2,3,4]:");
//        int yearLevelInNumber = input.nextInt();
//        input.close();
//
//        switch (yearLevelInNumber) {
//
//            case 1:
//                System.out.println("FRESHMAN");
//                break;
//
//            case 2:
//                System.out.println("SOPHOMORE");
//                break;
//
//            case 3:
//                System.out.println("JUNIOR");
//                break;
//
//            case 4:
//                System.out.println("SENIOR");
//                break;
//
//            default:
//                System.out.println("Invalid year level input");
//                break;
//
//        }

        boolean hasErrors = false, switchError = false;

        do {
            try {
                Scanner input = new Scanner(System.in);

                System.out.println("\n[Year Equivalent in Word]");

                System.out.print("Enter year level in number [1,2,3,4]: ");
                int yearLevelInNumber = input.nextInt();

                System.out.println("\n[Output]");

                switch (yearLevelInNumber) {
                    case 1:
                        System.out.println("FRESHMAN");
                        break;

                    case 2:
                        System.out.println("SOPHOMORE");
                        break;

                    case 3:
                        System.out.println("JUNIOR");
                        break;

                    case 4:
                        System.out.println("SENIOR");
                        break;

                    default:
                        System.out.println("Invalid year level input. Please input a number from 1-4 only.");
                        switchError = true;
                        break;
                }

                hasErrors = switchError ? true : false;
                input.nextLine();

            }

            catch (Exception e) {
                hasErrors = true;
                System.out.println("Invalid input. Please input number values (1-4) only.");
            }

            System.out.println();

        } while (hasErrors);

    }

}
