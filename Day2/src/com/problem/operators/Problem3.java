package com.problem.operators;

import java.util.Scanner;

public class Problem3 {

    public static void main(String[] args) {

//        Scanner input = new Scanner(System.in);
//
//        System.out.println("What is your name?");
//        String name = input.nextLine();
//
//        System.out.println("How many days have you worked?");
//        int days = input.nextInt();
//
//        System.out.println("How much is your daily rate?");
//        double rate = input.nextDouble();
//
//        double salary = Double.parseDouble(String.format("%.2f", days * rate));
//
//        System.out.println("Your name is: " + name);
//        System.out.println("Your salary is: " + salary);
//
//        input.close();

        boolean hasErrors = false;

        do {

            try {
                Scanner input = new Scanner(System.in);

                System.out.println("\n[Salary Calculator]");

                System.out.print("What is your name?: ");
                String name = input.nextLine();

                System.out.print("How many days have you worked?: ");
                int days = input.nextInt();

                System.out.print("How much is your daily rate?: ");
                double rate = input.nextDouble();

                double salary = Double.parseDouble(String.format("%.2f", days * rate));

                System.out.println("\n[Output]");
                System.out.println("Your name is: " + name);
                System.out.println("Your salary is: " + salary);

                input.close();
                hasErrors = false;
            }

            catch (Exception e) {
                hasErrors = true;
                System.out.println("Invalid inputs found.");
            }

            System.out.println();

        } while (hasErrors);
    }

}
