package com.problem.operators;

import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {

//        Scanner input = new Scanner(System.in);
//
//        System.out.println("Enter base:");
//        double base = input.nextDouble();
//
//        System.out.println("Enter height:");
//        double height = input.nextDouble();
//
//        double result = Double.parseDouble(String.format("%.2f", (base * height) / 2));
//        System.out.println("Area of Triangle: " + result);
//
//        input.close();

        boolean hasErrors = false;

        do {
            try {
                Scanner input = new Scanner(System.in);

                System.out.println("\n[Area of a Triangle calculator]");

                System.out.print("Enter base: ");
                double base = input.nextDouble();

                System.out.print("Enter height: ");
                double height = input.nextDouble();

                double result = Double.parseDouble(String.format("%.2f", (base * height) / 2));
                System.out.println("\n[Output]");
                System.out.println("Area of Triangle: " + result);

                input.close();
                hasErrors = false;
            }

            catch (Exception e) {
                hasErrors = true;
                System.out.println("Invalid input. Please input number values only.");
            }

            System.out.println();

        } while (hasErrors);

    }

}
