package com.problem.operators;

import java.util.Scanner;

public class Problem4 {

    public static void main(String[] args) {

//        Scanner input = new Scanner(System.in);
//
//        double dollarToPeso = 56.16;
//
//        System.out.println("Enter dollars:");
//        double inputDollar = input.nextDouble();
//
//        double result = dollarToPeso * inputDollar;
//
//        System.out.println("Dollar/s: $" + inputDollar);
//        System.out.println("Dollar/s in peso: PHP" + result);
//
//        input.close();

        boolean hasErrors = false;

        do {

            try {
                Scanner input = new Scanner(System.in);

                double dollarToPeso = 56.16;

                System.out.println("\n[Dollar to Peso Exchange Calculator]");

                System.out.print("Enter dollars: ");
                double inputDollar = input.nextDouble();

                double result = dollarToPeso * inputDollar;

                System.out.println("\n[Output]");
                System.out.println("Dollar/s: $ " + inputDollar);
                System.out.println("Dollar/s in peso: PHP " + result);

                input.close();
                hasErrors = false;
            }

            catch (Exception e) {
                hasErrors = true;
                System.out.println("Invalid input. Please input number values only.");
            }

            System.out.println();

        } while (hasErrors);

    }

}
