package com.problem.operators;

import java.util.Scanner;
import java.lang.Math;

public class Problem1 {

    public static void main(String[] args) {

//        Scanner input = new Scanner(System.in);
//
//        System.out.println("Enter radius:");
//        double radiusInput = input.nextInt();
//
//        double area = Double.parseDouble(String.format("%.2f", Math.PI * Math.pow(radiusInput, 2)));
//
//        System.out.println("The area is: " + area);
//
//        input.close();

        boolean hasError = false;



        do {

            Scanner input = new Scanner(System.in);

            try {
                System.out.println("\n[Area of a Circle Calculator]");

                System.out.print("Enter radius: ");
                double radiusInput = input.nextDouble();

                double area = Double.parseDouble(String.format("%.2f", Math.PI * Math.pow(radiusInput, 2)));

                System.out.println("\n[Output]");
                System.out.println("The area is: " + area);

                input.close();
                hasError = false;
            }

            catch (Exception e) {
                hasError = true;
                input.nextLine();
                System.out.println("Invalid input. Please input number values only.");
            }

            System.out.println();

        } while (hasError);

    }

}
