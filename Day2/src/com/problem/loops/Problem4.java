package com.problem.loops;

import java.util.Scanner;

public class Problem4 {

    public static void main(String[] args) {

        /*

            Problem 4
                - Write a program that will ask the user to give eight grades for eight subjects
                - The program will find average grade of the student from the eight subjects

         */

        Scanner input = new Scanner(System.in);

        System.out.print("Enter 8 grades separated by \",\": ");
        String[] grades = input.nextLine().split(",");

        double sum = 0, average = 0;

        for (String grade: grades)
            sum += Double.parseDouble(grade);

        average = sum / grades.length;

        System.out.println("Your grade average is: " + String.format("%.2f", average));

    }

}
