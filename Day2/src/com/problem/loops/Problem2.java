package com.problem.loops;

import java.util.Arrays;
import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {

        /*

            Problem 2
                - Write a program to generate a multiplication table.
                - The dimension is 10 x 10

         */

        Scanner input = new Scanner(System.in);

        // Get a range input from the user
        System.out.print("Enter a range: ");
        int range = input.nextInt();
        input.close();

        // Declare an empty array with a length = range
        int[] numbers = new int[range];

        // Generate a number from 1 to range and initialize it in the array
        for (int a = 0; a < range; a++) {
            numbers[a] = a + 1;
        }

        // Generate a mulitplication table with the provided range
        for (int i = 1; i <= range; i++) {

            for (int j = 0; j < range; j++) {
                System.out.print((numbers[j] * i) + "\t");
            }

            System.out.print("\n");

        }

    }

}
