package com.problem.loops;

import java.util.Arrays;
import java.util.Scanner;

public class Problem3 {

    public static void main(String[] args) {

        /*

            Problem 3
                - Write a program that will ask the user to give numbers
                - The program will find the total sum and average of five numbers using an array

         */

        Scanner input = new Scanner(System.in);

        System.out.print("Enter numbers (separated by \"-\": ");
        String[] numbers = input.nextLine().split("-");
        input.close();

        double sum = 0, average = 0;

        for (String num: numbers)
            // Convert each num to double to perform arithmetic operations
            sum += Double.parseDouble(num);

        average = sum / numbers.length;

        System.out.println("The sum of " + Arrays.toString(numbers) + " is: " + sum);
        System.out.println("The average is: " + String.format("%.2f", average));



    }

}
