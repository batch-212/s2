package com.problem.loops;

import java.util.Arrays;
import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {

        /*

            Problem1
                - Write a program to generate Pascal Triangle

         */

        System.out.print("Input number of rows: ");
        Scanner input = new Scanner(System.in);
        int numOfRows = input.nextInt();
        input.close();

        int c = 1;
        for (int i = 0; i < numOfRows; i++) {

            for (int space = 1; space <= numOfRows - i; space++) {
                System.out.print(" ");
            }

            for (int j = 0; j <= i; j++) {
                if (j == 0 || i == 0) {
                    c = 1;
                }
                else {
                    c = c * (i - j + 1) / j;
                }
                System.out.print(" " + c);
            }

            System.out.print("\n");
        }

    }

}
