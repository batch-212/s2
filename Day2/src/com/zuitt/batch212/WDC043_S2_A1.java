package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S2_A1 {

    public static void main(String[] args) {

        // Activity 1

        Scanner input = new Scanner(System.in);
        boolean hasErrors = false;

        do {
            try {
                System.out.println("\n[Leap Year Checker]");
                System.out.println("Input year to be checked if a leap year.");
                int inputYear = input.nextInt();

                boolean condition1 =
                    (inputYear % 4 == 0 && inputYear % 100 != 0)
                        ? true : false;
                boolean condition2 =
                    (inputYear % 4 == 0 && inputYear % 100 != 0 && inputYear % 400 == 0)
                        ? true : false;

                boolean isLeapYear = condition1 || condition2 ? true : false;

                System.out.println("[Output]");
                if (isLeapYear)
                    System.out.println(inputYear + " is a leap year");
                else
                    System.out.println(inputYear + " is NOT a leap year");

                hasErrors = false;
            }

            catch (Exception e) {
                hasErrors = true;
                input.nextLine();
                System.out.println("Invalid inputs.");
            }

        } while (hasErrors);
    }

}
