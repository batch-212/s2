package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WWDC043_S2_A2 {

    public static void main(String[] args) {

        // Array of prime numbers
        int[] fivePrimeNumbers = { 2, 3, 5, 7, 11 };
        System.out.println("The first prime number is: " + fivePrimeNumbers[0]);
        System.out.println("The second prime number is: " + fivePrimeNumbers[1]);
        System.out.println("The third prime number is: " + fivePrimeNumbers[2]);
        System.out.println("The fourth prime number is: " + fivePrimeNumbers[3]);
        System.out.println("The fifth prime number is: " + fivePrimeNumbers[4]);

        // Array List
        ArrayList<String> names = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("\nMy friends are: " + names);

        // Hash Maps
        HashMap<String, Integer> inventory = new HashMap<String, Integer>() {{
            put ("toothpaste", 15);
            put ("toothbrush", 20);
            put ("soap", 12);
        }};

        System.out.println("\nOur current inventory consists of: " + inventory);


    }

}
