package com.zuitt.batch212;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraySample {

    public static void main(String[] args) {

        /*

            Arrays
                - fixed/limited collections of data
                - 2^31 = 2,147,483,648 element capacity

         */

        // [Declaration of Array]
        // Method 1
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

        // Method 2
        int intSample[] = new int[2];

        intSample[0] = 50;
        intSample[1] = 100;

        System.out.println(intSample[0]);


        String stringArr[] = new String[3];

        stringArr[0] = "John";
        stringArr[1] = "Jane";
        stringArr[2] = "Joe";

        // Method 3
        int[] intArray2 = { 100, 200, 300, 400, 500 };

        System.out.println(intArray2);
        System.out.println(Arrays.toString(intArray2));

        System.out.println(Arrays.toString(stringArr));

        // [Array Methods]
        // sort()
        Arrays.sort(stringArr);
        System.out.println(Arrays.toString(stringArr));

        // binary search
        // - returns the index of the matched result
        // - returns negative if no result found and, its insertion point
        String searchTerm = "Zane";
        int binaryResult = Arrays.binarySearch(stringArr, searchTerm);
        System.out.println(binaryResult);


        // [Multidimensional Array]
        String[][] classRoom = new String[3][3];

        // first row
        classRoom[0][0] = "Dahyun";
        classRoom[0][1] = "Chaeyoung";
        classRoom[0][2] = "Nayeon";

        // second row
        classRoom[1][0] = "Luffy";
        classRoom[1][1] = "Zoro";
        classRoom[1][2] = "Sanji";

        // third row
        classRoom[2][0] = "Loid";
        classRoom[2][1] = "Yor";
        classRoom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classRoom));


        // [ArrayLists]
        //  - resizable arrays, wherein elements can be added or removed whenever it is needed
        // Declaration
        ArrayList<String> students = new ArrayList<>();

        // Adding elements
        students.add("John");
        students.add("Paul");

        System.out.println(students);

        // Access elements to an ArrayList
        System.out.println(students.get(0));

        // Update/changing of element
        students.set(1, "George");
        System.out.println(students);

        // Removing an element
        students.remove(1);
        System.out.println(students);

        // Removing all elements in an ArrayList
        students.clear();

        System.out.println(students);

        // Getting the length of an ArrayList
        System.out.println(students.size());

        // We can also declare and initialize values
        ArrayList<String> employees =
                new ArrayList<>(Arrays.asList("June", "Albert"));

        System.out.println(employees);


        // [HashMaps] -> key:value pair
        // Declaration
        HashMap<String, String> employeeRole = new HashMap<>();

        // Adding fields
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");

        System.out.println(employeeRole);

        // Retrieving field value
        System.out.println(employeeRole.get("Captain"));

        // Removing elements
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        // Retrieving hashMap keys
        System.out.println(employeeRole.keySet());

        // With Integers as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("english", 89);
        grades.put("math", 93);

        System.out.println(grades);

        // Hashmap with Array Lists
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75, 80, 90));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);

        System.out.println(subjectGrades);
    }

}
