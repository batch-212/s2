package com.zuitt.batch212;

import java.util.Scanner;

public class ControlStruct {

    public static void main(String[] args) {

        // [Operators]
        // 1. Arithmetic -> +,-,*,/,%
        // 2. Comparison -> >, >=, <, <=, ==, !=
        // 3. Assignment -> =, +=, etc.
        // 4. Logical -> &&, ||, !

        // [Conditional Statements]
        int num1 = 10, num2 = 20;

        // if statement
        if (num1 > 5)
            System.out.println(num1);

        // if-else statement
        if (num2 > 100)
            System.out.println("num2 is greater than 100");
        else
            System.out.println("num2 is less than 100");

        // if-else if and else statement
        if (num1 == 5)
            System.out.println("num1 is equal to 5");
        else if (num2 == 20)
            System.out.println("num2 is equal to 20");
        else
            System.out.println("anything else");


        // [Short Circuiting]
        // & and | (logical) - always evaluate both sides
        // && and || (Short Circuit) - doesn't complete the evaluation if it isn't necessary anymore
        //      - false && ...

        int x = 15, y = 0;
        if (y > 5 && x/y == 0)
            System.out.println("Result is: " + x/y);
        else
            System.out.println("The condition has short circuited");

//        System.out.println("test" == new String("test"));


        // [Switch Statement]
        int directionValue = 4;

        switch (directionValue) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
                break;
        }

    }

}
