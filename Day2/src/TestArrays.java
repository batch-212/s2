import java.util.Arrays;

public class TestArrays {

    public static void main(String[] args) {

        String[] colors = {"red","blue","green","cyan","magenta","yellow","key"};
        String[] rgb = new String[3];

        // arraycopy
        System.arraycopy(colors, 0, rgb, 0, 3);

        // copyOfRange()
        String[] cmyk = Arrays.copyOfRange(colors, 3, colors.length);

        System.out.println(Arrays.toString(colors));
        System.out.println(Arrays.toString(rgb));
        System.out.println(Arrays.toString(cmyk));

    }

}
